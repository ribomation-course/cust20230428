![Modern C++ Threads](./img/modern-cxx-threads.png)
# Modern C++ Threads, 3 days
###  28 April, 5 May, 12 May 2023

Welcome to this course about concurrent programming using threads in Modern C++.
In this repo, you will find installation instructions, source to demo programs and
solutions to the programming assignments (push:ed during the course).

# Links
* [Course Description](https://www.ribomation.se/programmerings-kurser/cxx/cxx-threads/)
* [Installation Instructions](./installation-instructions.md)


# Online Compilers
* [Compiler Explorer](https://godbolt.org/)
* [C++ Insights](https://cppinsights.io/)
* [C++ Quick Benchmarks](http://quick-bench.com/)
* [Coliru - Online Compiler](https://coliru.stacked-crooked.com/)
* [WandBox - Online Compiler](https://wandbox.org/)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
