#include <iostream>
#include <syncstream>
#include <string>
#include <chrono>
#include <future>
#include <thread>

namespace cr = std::chrono;
using namespace std::chrono_literals;
using namespace std::string_literals;
using std::cout;
using std::string;
using std::osyncstream;

int main() {
    auto start = cr::high_resolution_clock::now();
    auto elapsed = [start](string const& msg) {
        auto end  = cr::high_resolution_clock::now();
        auto time = cr::duration_cast<cr::milliseconds>(end - start);
        osyncstream{cout} << msg << ", elapsed " << time.count() << " ms\n";
    };

    cout << "[main] enter\n";
    auto f = std::async([] {
        std::this_thread::sleep_for(2.5s); return 42;
    });
    auto observable = f.share();
    elapsed("[main] after async");

    auto obs1 = std::jthread{[observable, &elapsed] {
        auto value = observable.get();
        elapsed("observer-1: "s + std::to_string(value));
    }};
    auto obs2 = std::jthread{[observable, &elapsed] {
        auto value = observable.get();
        elapsed("observer-2: "s + std::to_string(value));
    }};
    auto obs3 = std::jthread{[observable, &elapsed] {
        auto value = observable.get();
        elapsed("observer-3: "s + std::to_string(value));
    }};
    elapsed("[main] after threads");
}



