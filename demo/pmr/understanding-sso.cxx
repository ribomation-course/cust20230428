#include <iostream>
#include <string>
#include <memory_resource>

using std::pmr::string;
using std::pmr::null_memory_resource;
using std::cout;

int main() {
    auto sso = string{"123456789012345", null_memory_resource()};
    cout << "SSO [" << sso << "]\n";

    auto not_sso = string{"1234567890123456", null_memory_resource()};
    cout << "SSO [" << not_sso << "]\n";

    return 0;
}


