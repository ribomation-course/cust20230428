#pragma once

class Account {
    int balance = 0;
public:
    Account() = default;
    virtual ~Account() = default;
    Account(Account const&) = delete;
    auto operator =(Account const&) -> Account& = delete;

    virtual void update(int amount) {
        balance += amount;
    }

    int getBalance() const { return balance; }
};

