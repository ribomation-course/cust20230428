#include "account.hxx"
#include "atm.hxx"
#include <mutex>

class ThreadSafeAccount : public Account {
    std::mutex exclusive;
public:
    void update(int amount) override {
        auto guard = std::lock_guard<std::mutex>{exclusive};
        Account::update(amount);
    }
};

int main(int argc, char** argv) {
    auto atm = ATM<ThreadSafeAccount>{};
    atm.args(argc, argv);
    atm.run();
}

