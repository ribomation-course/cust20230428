#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <vector>
#include <thread>
#include <latch>
#include <syncstream>

namespace cr = std::chrono;
using std::cout;

int main() {
    auto const W = 5U;
    auto done = std::latch{W};
    auto rand_sleep = [](unsigned long seed){
        auto r = std::default_random_engine{seed};
        auto sleep_time = std::uniform_int_distribution<int>{10, 1000}(r);
        std::this_thread::sleep_for(cr::milliseconds(sleep_time));
    };

    auto worker = [&done, rand_sleep](unsigned id, unsigned long seed) {
        auto const tab = std::string(id * 5, ' ');
        std::osyncstream{cout} << tab << "[" << id << "]\n" << tab << "*\n";
        rand_sleep(seed);
        std::osyncstream{cout} << tab << "~\n";
        done.arrive_and_wait();
        std::osyncstream{cout} << tab << "#\n";
    };

    std::osyncstream{cout} << "[M]\n";
    {
        auto R = std::random_device{};
        auto workers = std::vector<std::jthread>{};
        workers.reserve(W);
        for (auto id = 1U; id <= W; ++id) workers.emplace_back(worker, id, R());
    }
    std::osyncstream{cout} << "~\n";
    done.wait();
    std::osyncstream{cout} << "#\n";
}
