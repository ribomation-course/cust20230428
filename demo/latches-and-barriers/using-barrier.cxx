#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <vector>
#include <thread>
#include <barrier>
#include <syncstream>

using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;
using std::string;
using std::osyncstream;

int main() {
    auto names = std::vector<string>{"Anna"s, "Berit"s, "Carin"s, "Doris"s};
    auto reporter = []() noexcept { osyncstream{cout} << "----\n"; };
    auto sync = std::barrier{static_cast<ptrdiff_t>(names.size()), reporter};

    auto worker = [&sync](string const& name, unsigned long seed) {
        osyncstream{cout} << "   " << name << "  woke up\n";

        sync.arrive_and_wait();
        osyncstream{cout} << "   " << name << "  came to the office\n";

        sync.arrive_and_wait();
        osyncstream{cout} << "   " << name << "  went for lunch\n";

        auto r = std::default_random_engine{seed};
        auto shopping = std::uniform_int_distribution<int>{0, 1};
        if (shopping(r) == 1) {
            osyncstream{cout} << "   " << name << "  GONE SHOPPING\n";
            sync.arrive_and_drop();
            return;
        }

        sync.arrive_and_wait();
        osyncstream{cout} << "   " << name << "  pushing paper\n";
        sync.arrive_and_wait();

        osyncstream{cout} << "   " << name << "  left the office\n";
        sync.arrive_and_wait();
    };

    osyncstream{cout} << "[main] enter\n";
    {
        auto R = std::random_device{};
        auto workers = std::vector<std::jthread>{};
        workers.reserve(names.size());
        for (auto const& name: names) workers.emplace_back(worker, name, R());
        osyncstream{cout} << "[main] waiting...\n";
    }
    osyncstream{cout} << "[main] exit\n";
}

