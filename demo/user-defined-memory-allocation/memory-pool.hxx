#pragma once

#include <bitset>
#include <memory>

namespace ribomation::memory {

    template<typename ElemType, unsigned CAPACITY>
    class MemoryPool {
        using byte = unsigned char;
        byte                    storage[CAPACITY * sizeof(ElemType)]{};
        std::bitset<CAPACITY>   busy{};
        unsigned                lastIndex = 0;
    public:
        bool empty() const { return busy.none(); }
        bool full()  const { return busy.all(); }

        auto allocate() -> ElemType* {
            if (full()) return nullptr;
            while (busy.test(lastIndex)) lastIndex = (lastIndex + 1) % CAPACITY;
            busy.set(lastIndex);
            void* address = storage + lastIndex * sizeof(ElemType);
            return reinterpret_cast<ElemType*>(address);
        }

        void deallocate(void* obj) {
            auto index = ((byte*) obj - (byte*) storage) / sizeof(ElemType);
            busy.reset(index);
        }

        template<typename ... ConstructorArgs>
        auto allocate(ConstructorArgs ... args) -> ElemType* {
            return std::construct_at(allocate(), args...);
        }

        void deallocate(ElemType* obj) {
            std::destroy_at(obj);
            deallocate(reinterpret_cast<void*>(obj));
        }

    };

}

