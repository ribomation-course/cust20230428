#include <iostream>
#include <string>
#include <utility>
#include <cassert>
#include <memory>
#include "memory-pool.hxx"

using std::cout;
using std::string;
using namespace std::string_literals;

class Person {
    string name{};
    unsigned age{};
public:
    Person() { cout << "+Person() @ " << this << "\n"; }
    Person(string name_, unsigned int age_) : name(std::move(name_)), age(age_) {
        cout << "+Person(" << name << ", " << age << ") @ " << this << "\n";
    }
    ~Person() { cout << "~Person() @ " << this << "\n"; }
    void setName(string name_) { name = std::move(name_); }
    void setAge(unsigned age_) { age = age_; }
    friend std::ostream& operator<<(std::ostream& os, const Person& person) {
        return os << "name: " << person.name << ", age: " << person.age;
    }
};

int main() {
    using ribomation::memory::MemoryPool;

    auto mem = MemoryPool<Person, 3>{};
    assert(mem.empty());
    {
        auto ptr1 = new (mem.allocate()) Person{};
        ptr1->setName("Anna");
        ptr1->setAge(22);
        cout << "ptr1: " << *ptr1 << " @ " << ptr1 << "\n";
        assert(!mem.empty());

        auto ptr2 = mem.allocate("Berit"s, 32);
        cout << "ptr2: " << *ptr2 << " @ " << ptr2 << "\n";

        struct Disposer {
            MemoryPool<Person, 3>& mem;
            Disposer(MemoryPool<Person, 3>& mem) : mem(mem) {}
            void operator()(Person* ptr) { mem.deallocate(ptr); }
        };

        auto ptr3 = std::unique_ptr<Person, Disposer>{mem.allocate("Carin"s, 42), Disposer{mem}};
        cout << "ptr3: " << *ptr3 << " @ " << ptr3.get() << "\n";

        assert(mem.allocate() == nullptr);
        assert(mem.full());

        mem.deallocate(ptr2);
        assert(!mem.full());
        mem.deallocate(ptr1);
    }
    assert(mem.empty());
}

