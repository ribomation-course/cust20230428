#include <iostream>
#include <thread>
#include <syncstream>
using std::cout;

int main() {
    auto t1 = std::jthread{[]() { cout << "[thread] Hello from a C++ thread\n"; }};
    auto t2 = std::jthread{};

    std::swap(t1, t2);
    std::osyncstream{cout}
            << std::boolalpha
            << "[main] t1.joinable=" << t1.joinable()
            << ", t2.joinable=" << t2.joinable() << "\n";

    cout << "[main] before t2.join\n";
    t2.join();
    cout << "[main] exit\n";
}

