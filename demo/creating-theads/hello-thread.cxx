#include <iostream>
#include <thread>
using std::cout;

int main() {
    cout << "[main] before thread\n";
    {
        auto body = []() {
            cout << "[thread] Hello from a C++ thread\n";
        };
        auto thr = std::jthread{body};
    }
    cout << "[main] after thread\n";
}

