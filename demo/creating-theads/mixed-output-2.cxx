#include <iostream>
#include <string>
#include <thread>
#include <syncstream>
using std::cout;

int main() {
    auto body = [](unsigned id, unsigned n) {
        auto const tab = std::string((id - 1) * 5, ' ');
        do {
            std::osyncstream{cout} << tab << "[t-" << id << "] "
                                   << "message #" << n << "\n";
        } while (--n > 0);
    };
    {
        auto const N = 100;
        auto t1 = std::jthread{body, 1, N};
        auto t2 = std::jthread{body, 2, N};
        auto t3 = std::jthread{body, 3, N};
        auto t4 = std::jthread{body, 4, N};
    }
    cout << "[main] exit\n";
}

