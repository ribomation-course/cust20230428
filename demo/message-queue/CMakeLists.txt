cmake_minimum_required(VERSION 3.22)
project(message_queue)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(message-queue-unbounded-test
        message-queue-unbounded-test.cxx
        message-queue-unbounded.hxx)
target_compile_options(message-queue-unbounded-test PRIVATE ${WARN})

add_executable(message-queue-bounded-test
        message-queue-bounded-test.cxx
        message-queue-bounded.hxx)
target_compile_options(message-queue-bounded-test PRIVATE ${WARN})


