#include <iostream>
#include <thread>
#include <chrono>
using namespace std::chrono_literals;
using std::cout;

int main() {
    cout << "\n[main] enter\n";

    auto thr = std::jthread{[](std::stop_token stopToken) {
        cout << "[thread] enter\n";
        do {
            cout << "." << std::flush;
            std::this_thread::sleep_for(.25s);
            if (stopToken.stop_requested()) {
                cout << "[thread] OK, time to quit\n";
                return;
            }
        } while (true);
    }};

    cout << "[main] before sleep\n";
    std::this_thread::sleep_for(2s);
    cout << "\n[main] exit\n";
}
