#include <iostream>
#include <thread>
#include <chrono>
using namespace std::chrono_literals;
using std::cout;

int main() {
    auto thr = std::jthread{[](std::stop_token stopToken) {
        auto stopCallback = std::stop_callback{stopToken, [](){
            cout << "[thread] OK, time to quit\n";
        }};
        cout << "[thread] enter\n";
        do {
            cout << "." << std::flush;
            std::this_thread::sleep_for(.25s);
            if (stopToken.stop_requested()) break;
        } while (true);
    }};

    cout << "[main] before sleep\n";
    std::this_thread::sleep_for(2s);
    cout << "\n[main] exit\n";
}

