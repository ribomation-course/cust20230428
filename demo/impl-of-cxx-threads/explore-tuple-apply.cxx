#include <iostream>
#include <tuple>
#include <functional>
#include <pthread.h>
#include <unistd.h>

using std::cout;
using std::tuple;
using std::tuple_size;
using std::make_tuple;
using std::apply;
using std::function;

//template<typename... Args>
//void print(tuple<Args...> const& t) {
////    auto const N = tuple_size<Tuple>{};
////    cout << "[print] t.size=" << N << "\n";
//    auto pr = [](Args const& ... arg) {
//        auto n = 0UL;
//        cout << "[";
//        ((cout << arg << (++n < sizeof...(Args) ? ", " : "")), ...);
//        cout << "]\n";
//    };
//    std::apply(pr, t);
//}

//template<typename TaskFn, typename... Args>
//void ASYNC(TaskFn taskFn, Args... args) {
//    function<void()> bound = [task = std::move(taskFn), args = make_tuple(std::move(args)...)]() {
//        apply(task, args);
//    };
//
//    cout << "[ASYNC] before\n";
//    bound();
//    cout << "[ASYNC] after\n";
//
//    decltype(bound) pkg = std::move(bound);
//    cout << "[ASYNC] pkg\n";
//    pkg();
//    cout << "[ASYNC] pkg 2\n";
//}

struct Thread {
    pthread_t runner;
    function<void()> body;

    static void* wrapper(Thread* self) {
        (*self).body();
        return nullptr;
    }

    template<typename BodyFn, typename... Args>
    Thread(BodyFn bodyFn, Args... args) {
        body = std::move([bodyFn = std::move(bodyFn), args = make_tuple(std::move(args)...)] {
            std::apply(bodyFn, args);
        });
        pthread_create(&runner, nullptr, reinterpret_cast<void* (*)(void*)>(wrapper), this);
    }

    ~Thread() {
        pthread_join(runner, nullptr);
    }
};


int main() {
//    ASYNC([](int id, int val) {
//        cout << "[task-" << id << "] val=" << val << "\n";
//    }, 1, 42);

    {
        auto A = [](int id, int val) {
            cout << "[thread-" << id << "] val=" << val << "\n";
            sleep(1);
            cout << "[thread-" << id << "] val=" << val << " done\n";
        };
        auto t1 = Thread{A, 1, 42};
        auto B = [](int id, int val) {
            cout << "[thread-" << id << "] val=" << val << "\n";
            sleep(1);
            cout << "[thread-" << id << "] val=" << val << " done\n";
        };
        auto t2 = Thread{B, 2, 17};
    }
    cout << "[main] done\n";
}