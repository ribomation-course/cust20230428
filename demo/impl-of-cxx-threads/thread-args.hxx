#pragma once

#include <functional>
#include <tuple>
#include <pthread.h>

namespace ribomation::concurrent {

    class ThreadArgs {
        pthread_t runner{};
        std::function<void()> body;
        static void* wrapper(ThreadArgs* t) {
            t->body();
            return nullptr;
        }
    public:
        template<typename BodyFn, typename... Args>
        explicit ThreadArgs(BodyFn bodyFn, Args... args) {
            body = std::move([bodyFn = std::move(bodyFn), args = std::make_tuple(std::move(args)...)] {
                std::apply(bodyFn, args);
            });
            pthread_create(&runner, nullptr, reinterpret_cast<void* (*)(void*)>(wrapper), this);
        }
        ~ThreadArgs() { join(); }
        void join() const { pthread_join(runner, nullptr); }
    };

}

