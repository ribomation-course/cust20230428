#include <iostream>
#include <sstream>
#include <unistd.h>
#include "thread.hxx"
#include "mutex.hxx"
#include "condition.hxx"

using std::cout;
namespace rm = ribomation::concurrent;

template<typename PayloadType>
class Mailbox {
    rm::Mutex exclusive{};
    rm::Condition notEmpty{exclusive};
    rm::Condition notFull{exclusive};
    PayloadType payload{};
    bool full = false;
public:
    void put(PayloadType msg) {
        auto g = rm::LockGuard{exclusive};
        notFull.wait([this] { return !full; });
        payload = msg;
        full = true;
        notEmpty.notifyAll();
    }
    PayloadType get() {
        auto g = rm::LockGuard{exclusive};
        notEmpty.wait([this] { return full; });
        auto msg = payload;
        payload = {};
        full = false;
        notFull.notifyAll();
        return msg;
    }
};

int main() {
    cout << "[main] enter\n";
    auto mb = Mailbox<long>{};
    {
        auto consumer = rm::Thread{[&mb] {
            for (auto msg = mb.get(); msg != 0; msg = mb.get()) {
                auto buf = std::ostringstream{};
                buf << "[consumer] " << msg << "\n";
                cout << buf.str();
            }
            cout << "[consumer] done\n";
        }};
        for (auto k = 1L; k < 100L; ++k) mb.put(k);
        cout << "[main] sent\n";
        mb.put(0);
    }
    cout << "[main] exit\n";
}

