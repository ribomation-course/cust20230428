#include <iostream>
#include <syncstream>
#include <unistd.h>
#include "thread-args.hxx"

using std::cout;
namespace rm = ribomation::concurrent;

int main() {
    cout << "[main] enter\n";
    auto body = [](unsigned id, unsigned N) {
        std::osyncstream{cout} << "thread-" << id << " started\n";
        for (auto k = 0U; k < N; ++k) {
            std::osyncstream{cout} << "thread-" << id << " k=" << k+1 << "\n";
            usleep(k * 10 * 1000);
        }
        std::osyncstream{cout} << "thread-" << id << " done\n";
    };
    {
        auto t1 = rm::ThreadArgs{body, 1, 10};
        auto t2 = rm::ThreadArgs{body, 2, 15};
        auto t3 = rm::ThreadArgs{body, 3, 8};
    }
    cout << "[main] exit\n";
}


