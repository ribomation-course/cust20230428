#include <iostream>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <execution>

using std::cout;

auto operator <<(std::ostream& os, std::vector<int> const& v) -> std::ostream& {
    os << "[";
    for (auto e: v) { cout << e << " "; }
    os << "]";
    return os;
}

void usecase_original() {
    auto v = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    cout << "[orig] before " << v << "\n";
    try {
        std::for_each(v.begin(), v.end(), [](int& e) {
            if (e == 6) throw std::invalid_argument{"oops"};
            ++e;
        });
    } catch (std::exception& x) {
        cout << "[orig] failed: " << x.what() << "\n";
    }
    cout << "[orig] after " << v << "\n";
}

void usecase_parallel() {
    auto v = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    cout << "[par] before " << v << "\n";
    try {
        using std::execution::par;
        std::for_each(par, v.begin(), v.end(), [](int& e) {
            if (e == 6) throw std::invalid_argument{"oops"};
            ++e;
        });
    } catch (std::exception& x) {
        cout << "[par] failed: " << x.what() << "\n";
    }
    cout << "[par] after " << v << "\n";
}

int main() {
    usecase_original();
    usecase_parallel();
}
