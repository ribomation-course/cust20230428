#include <iostream>
#include <vector>
#include <algorithm>
#include <execution>
#include <chrono>
#include <functional>
#include <string>
#include <random>
#include <tuple>

namespace cr = std::chrono;
using std::cout;
using std::string;
using std::tuple;
using std::make_tuple;

auto load(unsigned N) -> std::vector<int> {
    using namespace std::numbers;
    auto R = std::random_device{};
    auto r = std::default_random_engine{R()};
    auto D = std::uniform_int_distribution<>{-1000, 1000};
    auto v = std::vector<int>{};
    v.reserve(N);
    for (auto k = 0U; k < N; ++k) v.push_back(D(r));
    return v;
}

void benchmark(string const& name, std::function<tuple<int, int>()> target) {
    auto start = cr::high_resolution_clock::now();
    auto [min, max] = target();
    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::microseconds>(end - start);
    cout << name << ": min/max=" << min << "/" << max << ", elapsed " << elapsed.count() << " us\n";
}

void suite(unsigned N) {
    using namespace std::string_literals;
    using namespace std::execution;

    cout << "-- N = " << N << " ------\n";
    auto V = load(N);
    benchmark("[orig]     "s, [v = V]() mutable {
         std::sort(v.begin(), v.end());
         return make_tuple(v.front(), v.back());
    });
    benchmark("[seq]      "s, [v = V]() mutable {
        std::sort(seq, v.begin(), v.end());
        return make_tuple(v.front(), v.back());
    });
    benchmark("[par]      "s, [v = V]() mutable {
        std::sort(par, v.begin(), v.end());
        return make_tuple(v.front(), v.back());
    });
    benchmark("[unseq]    "s, [v = V]() mutable {
        std::sort(unseq, v.begin(), v.end());
        return make_tuple(v.front(), v.back());
    });
    benchmark("[par_unseq]"s, [v = V]() mutable {
        std::sort(par_unseq, v.begin(), v.end());
        return make_tuple(v.front(), v.back());
    });
}

int main() {
    for (auto n = 100U; n <= 1'000'000; n *= 10) suite(n);
}

