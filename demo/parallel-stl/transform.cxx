#include <iostream>
#include <vector>
#include <algorithm>
#include <execution>
#include <chrono>
#include <functional>
#include <string>
#include <random>
#include <numbers>

namespace cr = std::chrono;
using std::cout;
using std::string;

auto load(unsigned N) -> std::vector<double> {
    using namespace std::numbers;
    auto R = std::random_device{};
    auto r = std::default_random_engine{R()};
    auto D = std::uniform_real_distribution<>{-pi / 2., +pi / 2.};
    auto v = std::vector<double>{};
    v.reserve(N);
    for (auto k = 0U; k < N; ++k) v.push_back(D(r));
    return v;
}

void benchmark(string const& name, std::function<void()> target) {
    auto start = cr::high_resolution_clock::now();
    target();
    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::microseconds>(end - start);
    cout << name << ": " << elapsed.count() << " us\n";
}

void suite(unsigned N) {
    using namespace std::string_literals;
    using namespace std::execution;

    cout << "-- N = " << N << " ------\n";
    benchmark("[orig]     "s, [v = load(N)]() mutable {
        std::transform(v.begin(), v.end(), v.begin(), [](double x) { return ::cos(x); });
    });
    benchmark("[seq]      "s, [v = load(N)]() mutable {
        std::transform(seq, v.begin(), v.end(), v.begin(), [](double x) { return ::cos(x); });
    });
    benchmark("[par]      "s, [v = load(N)]() mutable {
        std::transform(par, v.begin(), v.end(), v.begin(), [](double x) { return ::cos(x); });
    });
    benchmark("[unseq]    "s, [v = load(N)]() mutable {
        std::transform(unseq, v.begin(), v.end(), v.begin(), [](double x) { return ::cos(x); });
    });
    benchmark("[par_unseq]"s, [v = load(N)]() mutable {
        std::transform(par_unseq, v.begin(), v.end(), v.begin(), [](double x) { return ::cos(x); });
    });
}

int main() {
    for (auto n = 100U; n <= 1'000'000; n *= 10) suite(n);
}

