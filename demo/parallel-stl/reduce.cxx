#include <iostream>
#include <vector>
#include <algorithm>
#include <execution>
#include <chrono>
#include <functional>
#include <string>
#include <random>
#include <numbers>

namespace cr = std::chrono;
using std::cout;
using std::string;

auto load(unsigned N) -> std::vector<double> {
    using namespace std::numbers;
    auto R = std::random_device{};
    auto r = std::default_random_engine{R()};
    auto D = std::uniform_real_distribution<>{0.95, 1.05};
    auto v = std::vector<double>{};
    v.reserve(N);
    for (auto k = 0U; k < N; ++k) v.push_back(D(r));
    return v;
}

void benchmark(string const& name, std::function<long double()> target) {
    auto start = cr::high_resolution_clock::now();
    auto result = target();
    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::microseconds>(end - start);
    cout << name << ": result=" << result << ", elapsed " << elapsed.count() << " us\n";
}

void suite(unsigned N) {
    using namespace std::string_literals;
    using namespace std::execution;

    cout << "-- N = " << N << " ------\n";
    auto V = load(N);
    benchmark("[orig]     "s, [v = V]() mutable {
        return std::accumulate(v.begin(), v.end(), 1.0L, [](double a, double b) { return a * b; });
    });
    benchmark("[seq]      "s, [v = V]() mutable {
        return std::reduce(seq, v.begin(), v.end(), 1.0L, [](double a, double b) { return a * b; });
    });
    benchmark("[par]      "s, [v = V]() mutable {
        return std::reduce(par, v.begin(), v.end(), 1.0L, [](double a, double b) { return a * b; });
    });
    benchmark("[unseq]    "s, [v = V]() mutable {
        return std::reduce(unseq, v.begin(), v.end(), 1.0L, [](double a, double b) { return a * b; });
    });
    benchmark("[par_unseq]"s, [v = V]() mutable {
        return std::reduce(par_unseq, v.begin(), v.end(), 1.0L, [](double a, double b) { return a * b; });
    });
}

int main() {
    for (auto n = 100U; n <= 1'000'000; n *= 10) suite(n);
}

