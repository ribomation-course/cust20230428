#pragma once
#include "mutex.hxx"
#include "condition.hxx"

namespace ribomation::concurrent {
    namespace rm = ribomation::concurrent;

    template<typename PayloadType>
    class Mailbox {
        rm::Mutex exclusive{};
        rm::Condition notEmpty{exclusive};
        rm::Condition notFull{exclusive};
        PayloadType payload{};
        bool full = false;
    public:
        void put(PayloadType msg) {
            auto g = rm::LockGuard{exclusive};
            notFull.wait([this] { return !full; });
            payload = msg;
            full = true;
            notEmpty.notifyAll();
        }
        PayloadType get() {
            auto g = rm::LockGuard{exclusive};
            notEmpty.wait([this] { return full; });
            auto msg = payload;
            payload = {};
            full = false;
            notFull.notifyAll();
            return msg;
        }
    };

}


