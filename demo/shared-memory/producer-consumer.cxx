#include <iostream>
#include <array>
#include <cstdlib>
#include <sys/wait.h>
#include "shared-memory.hxx"
#include "mailbox.hxx"
namespace rm = ribomation::concurrent;
using std::cout;

int main() {
    auto shm       = rm::SharedMemory{sizeof(rm::Mailbox<long>)};
    auto mbStorage = shm.allocate<rm::Mailbox<long>>();
    auto mb        = new (mbStorage) rm::Mailbox<long>{};

    auto rc = fork();
    if (rc == -1) return 1;
    if (rc == 0) {
        cout << "[child] start\n";
        for (auto k = 1; k <= 100; ++k) mb->put(k);
        mb->put(0L);
        cout << "[child] done\n";
        exit(0);
    }

    cout << "[parent] start\n";
    for (auto msg = mb->get(); msg != 0L; msg = mb->get()) {
        cout << "[parent] msg=" << msg << "\n";
    }
    wait(nullptr);
    cout << "[parent] Mailbox destructor\n";
    mb->~Mailbox();
    cout << "[parent] done\n";
}

