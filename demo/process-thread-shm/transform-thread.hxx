#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <syncstream>
#include <memory_resource>
#include <cctype>
#include "message-queue.hxx"
#include "message.hxx"


namespace ribomation::app {
    namespace m = std::pmr;
    namespace co = ribomation::concurrent;
    using std::string;

    struct Transformer {
        unsigned id;
        co::MessageQueue<Message*>* inbox;
        co::MessageQueue<Message*>* outbox;

        Transformer(unsigned id_, co::MessageQueue<Message*>* inbox_, co::MessageQueue<Message*>* outbox_)
                : id{id_}, inbox{inbox_}, outbox{outbox_} {}

        void run() {
            auto const swe_chars = string{"åäöÅÄÖ"};
            auto letter = [swe_chars](char ch) -> bool {
                return ::isalpha(ch) || swe_chars.find(ch) != string::npos;
            };

            auto msg = inbox->get();
            for (; not msg->last; msg = inbox->get()) {
//                std::osyncstream{std::cout} << "Transformer-" << id << ": '" << msg->getText() << "'\n";
//                std::osyncstream{std::cout} << "Transformer-" << id
//                                            << ": [" << inbox->count() << "] [" << outbox->count() << "]\n";

//                for (auto k = 0U; k < msg->length; ++k) {
//                    char ch = msg->payload[k];
//                    if (not letter(ch)) {
//                        msg->payload[k] = ' ';
//                    } else {
//                        msg->payload[k] = ::toupper(ch);
//                    }
//                }

                auto last = std::remove_if(msg->payload.begin(), msg->payload.begin() + msg->length, [letter](char ch) {
                    return not (letter(ch) || ch == ' ');
                });
                msg->length = std::distance(msg->payload.begin(), last);
                outbox->put(msg);
            }
            outbox->put(msg);
            std::osyncstream{std::cout} << "Transformer-" << id << " done\n";
        }
    };

}

