#pragma once

#include <iostream>
#include <syncstream>
#include <string>
#include <memory_resource>
#include "file-slurper.hxx"
#include "message-queue.hxx"
#include "message.hxx"

namespace ribomation::app {
    namespace m = std::pmr;
    namespace co = ribomation::concurrent;
    namespace io = ribomation::io;
    using std::string;

    struct Loader {
        string const filename;
        co::MessageQueue<Message*>* outbox;
        m::synchronized_pool_resource* heap;

        Loader(string filename_, co::MessageQueue<Message*>* outbox_, m::synchronized_pool_resource* heap_)
                : filename{filename_}, outbox{outbox_}, heap{heap_} {}

        void run() {
            auto id = 0L;
            for (auto const& line: io::FileSlurper{filename}) {
                if (line.empty()) continue;
                //std::osyncstream{std::cout} << "Loader '" << line << "' ["<<outbox->count() << "] \n";

                auto mem = heap->allocate(sizeof(Message));
                auto obj = new(mem) Message{++id, line};
                outbox->put(obj);
            }

            outbox->put(new(heap->allocate(sizeof(Message))) Message{id});
            std::osyncstream{std::cout} << "Loader '" << filename << "' done\n";
        }
    };

}

