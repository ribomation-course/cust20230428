#include <stdio.h>
#include <pthread.h>

typedef struct sAccount {
    pthread_mutex_t exclusive;
    int balance;
} Account;

void account_construct(Account* this) {
    pthread_mutexattr_t attrs;
    pthread_mutexattr_init(&attrs);
    pthread_mutexattr_settype(&attrs, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&(this->exclusive), &attrs);
    pthread_mutexattr_destroy(&attrs);
    this->balance = 0;
}

void account_destruct(Account* this) {
    pthread_mutex_destroy(&(this->exclusive));
}

void account_update(Account* this, int amount) {
    pthread_mutex_lock(&(this->exclusive));
    this->balance += amount;
    pthread_mutex_unlock(&(this->exclusive));
}

int account_balance(Account* this) {
    pthread_mutex_lock(&(this->exclusive));
    int result = this->balance;
    pthread_mutex_unlock(&(this->exclusive));
    return result;
}


typedef struct sUpdaterParams {
    unsigned id;
    Account* the_account;
    unsigned num_updates;
} UpdaterParams;

void* UpdaterBody(void* args) {
    UpdaterParams* params = (UpdaterParams*) args;
    printf("[updater-%d] started\n", params->id);
    for (unsigned k = 0; k < params->num_updates; ++k) {
        account_update(params->the_account, +100);
    }
    printf("[updater-%d] middle\n", params->id);
    for (unsigned k = 0; k < params->num_updates; ++k) {
        account_update(params->the_account, -100);
    }
    printf("[updater-%d] done\n", params->id);
    return NULL;
}

int main() {
    printf("[main] enter\n");
    Account the_account;
    account_construct(&the_account);
    printf("init balance = %d\n", account_balance(&the_account));

    unsigned const U = 5;
    pthread_t updaters[U];
    UpdaterParams params[U];
    for (unsigned k = 0; k < U; ++k) {
        params[k].id = k + 1;
        params[k].num_updates = 1000;
        params[k].the_account = &the_account;
        pthread_create(&updaters[k], NULL, UpdaterBody, &params[k]);
    }
    for (unsigned k = 0; k < U; ++k) {
        pthread_join(updaters[k], NULL);
    }

    printf("final balance = %d\n", account_balance(&the_account));
    account_destruct(&the_account);
    printf("[main] exit\n");
}


