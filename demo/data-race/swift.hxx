#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <syncstream>
#include <thread>

using std::osyncstream, std::cout;

template<typename MoneyboxType>
class SWIFT {
    unsigned num_transactions = 1'000;
    int amount = 1;
    int const STOP = -1;

public:
    void args(int argc, char** argv) {
        using namespace std::string_literals;
        for (auto k = 1; k < argc; ++k) {
            auto arg = std::string{argv[k]};
            if (arg == "-t"s) num_transactions = std::stoi(argv[++k]);
            else if (arg == "-a"s) amount = std::stoi(argv[++k]);
            else {
                std::cerr << "usage: " << argv[0] << " [-t <int>] [-a <int>]\n";
                throw std::invalid_argument{"option "s + arg};
            }
        }
        using std::cout;
        cout << "# transactions: " << num_transactions << "\n";
        cout << "amount        : " << amount << " kr\n";
    }

    void run() {
        auto the_money_box = MoneyboxType{};

        auto receiver = [&the_money_box, this]() {
            cout << "[receiver] started\n";
            auto count = 0;
            auto sum = 0;
            for (int value = the_money_box.recv(); value != STOP; value = the_money_box.recv()) {
                sum += value;
                ++count;
            }
            osyncstream{cout} << "[receiver] received    = " << sum << " kr\n";
            osyncstream{cout} << "[receiver] # transfers = " << count << "\n";
        };

        auto sender = [&the_money_box, this]() {
            cout << "[sender] started\n";
            for (auto k = 0U; k < num_transactions; ++k) the_money_box.send(amount);
            the_money_box.send(STOP);
            osyncstream{cout} << "[sender] sent = " << (num_transactions * amount) << " kr\n";
        };

        cout << "[main] enter\n";
        {
            auto recvThr = std::jthread{receiver};
            auto sendThr = std::jthread{sender};
        }
        cout << "[main] exit\n";
    }
};
