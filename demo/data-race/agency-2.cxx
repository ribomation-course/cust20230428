#include "money-box.hxx"
#include "swift.hxx"
#include <condition_variable>

class ThreadSafeMoneybox {
    std::mutex exclusive;
    std::condition_variable not_empty{};
    std::condition_variable not_full{};
    int payload = 0;
    bool full = false;
public:
    void send(int amount) {
        {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_full.wait(guard, [this] { return !full; });
            payload = amount;
            full = true;
        }
        not_empty.notify_one();
    }
    int recv() {
        decltype(payload) data{};
        {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_empty.wait(guard, [this] { return full; });
            data = payload;
            payload = {};
            full = false;
        }
        not_full.notify_one();
        return data;
    }
};

int main(int argc, char** argv) {
    auto swift = SWIFT<ThreadSafeMoneybox>{};
    swift.args(argc, argv);
    swift.run();
}

