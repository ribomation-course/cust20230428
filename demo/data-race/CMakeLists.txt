cmake_minimum_required(VERSION 3.22)
project(data_race)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(agency money-box.hxx swift.hxx agency.cxx)
target_compile_options(agency PRIVATE ${WARN})

add_executable(agency-2 money-box.hxx swift.hxx agency-2.cxx)
target_compile_options(agency-2 PRIVATE ${WARN})


