cmake_minimum_required(VERSION 3.22)
project(atomic)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(bank bank.cxx)
target_compile_options(bank PRIVATE ${WARN})


