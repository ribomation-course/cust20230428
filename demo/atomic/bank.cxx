#include <iostream>
#include <syncstream>
#include <thread>
#include <atomic>

using std::cout;
using std::osyncstream;

class Account {
    std::atomic_int balance;
public:
    Account(int b = 0) : balance{b} {}
    int getBalance() const {
        return balance.load();
    }
    void update(int amount) {
        balance.fetch_add(amount);
    }
};

int main() {
    auto const N     = 100'000U;
    auto the_account = Account{};
    auto updaterFn   = [&the_account](unsigned id) {
        osyncstream{cout} << "updater-" << id << " started: balance=" << the_account.getBalance() << "\n";
        for (auto k = 0U; k < N; ++k) the_account.update(+100);
        for (auto k = 0U; k < N; ++k) the_account.update(-100);
        osyncstream{cout} << "updater-" << id << " done: balance=" << the_account.getBalance() << "\n";
    };
    {
        auto u1 = std::jthread{updaterFn, 1};
        auto u2 = std::jthread{updaterFn, 2};
        auto u3 = std::jthread{updaterFn, 3};
        auto u4 = std::jthread{updaterFn, 4};
        auto u5 = std::jthread{updaterFn, 5};
    }
    cout << "[main] balance=" << the_account.getBalance() << "\n";
}
