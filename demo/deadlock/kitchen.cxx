#include <iostream>
#include <string>
#include <syncstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <utility>
#include <chrono>

using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;
using std::osyncstream;
using std::string;

class Resource {
    const string name;
    bool busy = false;
    std::mutex exclusive{};
    std::condition_variable not_busy{};
public:
    explicit Resource(string name_) : name{(std::move(name_))} {}
    [[nodiscard]] const string& getName() const { return name; }

    void acquire() {
        auto g = std::unique_lock{exclusive};
        not_busy.wait(g, [this] { return !busy; });
        busy = true;
    }
    void release() {
        {
            auto g = std::lock_guard{exclusive};
            busy = false;
        }
        not_busy.notify_all();
    }
};

class Cook {
    const string name;
    Resource& r1;
    Resource& r2;
public:
    explicit Cook(string name_, Resource& r1_, Resource& r2_)
            : name{(std::move(name_))}, r1{r1_}, r2{r2_} {}

    [[noreturn]] void operator ()() {
        do {
            osyncstream{cout} << "[" << name << "] want " << r1.getName() << "\n";
            r1.acquire();
            osyncstream{cout} << "[" << name << "] acquired " << r1.getName() << "\n";

            osyncstream{cout} << "[" << name << "] want " << r2.getName() << "\n";
            r2.acquire();
            osyncstream{cout} << "[" << name << "] acquired " << r2.getName() << "\n";

            osyncstream{cout} << "[" << name << "] using resources\n";
            std::this_thread::sleep_for(0.5s);

            osyncstream{cout} << "[" << name << "] release " << r2.getName() << "\n";
            r2.release();

            osyncstream{cout} << "[" << name << "] release " << r1.getName() << "\n";
            r1.release();
        } while (true);
    }
};

int main() {
    auto knife = Resource{"knife"};
    auto cutting_board = Resource{"cutting-board"};
    auto werner_1 = std::jthread{Cook{"Werner 1", knife, cutting_board}};
//    auto werner_2 = std::jthread{Cook{"        Werner 2", cutting_board, knife}};
    auto werner_2 = std::jthread{Cook{"        Werner 2", knife, cutting_board}};
}

