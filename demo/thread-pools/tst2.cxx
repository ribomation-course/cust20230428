#include <iostream>
#include <thread>
#include <syncstream>
#include "thread-pool-simple.hxx"

using namespace std::chrono_literals;
namespace rm = ribomation::concurrent;
using std::cout;
using std::osyncstream;

int main() {
    {
        auto pool = rm::ThreadPool{};
        for (auto id = 1; id <= 10; ++id) {
            pool.submit([ID = id] {
                osyncstream{cout} << "task-" << ID << " start\n";
                std::this_thread::sleep_for(2s);
                osyncstream{cout} << "task-" << ID << " done\n";
            });
        }
        cout << "[main] before shutdown\n";
    }
    cout << "[main] done\n";
}

