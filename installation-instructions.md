# Installation Instructions

To perform the programming exercises, you need:
* Modern C++ compiler, supporting C++20, _to compile your programs_
* Decent C++ IDE, _to write your code_
* GIT client, _to easily get the solutions from this repo_


## Installation of Ubuntu Linux
We will do the exercises on Linux. Therefore, you need access to a Linux or Unix system, preferably Ubuntu.
Read our common guide of how to install Linux on WSL and a C++ compiler.

* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)


## Installation within Ubuntu Linux
## The GNU C/C++ compiler version 12 and builder tools

    sudo apt install make cmake g++-12 gdb valgrind

_N.B._, If you cannot get version 12, try version 11, _i.e._, `g++-11`

## Additional tools

    sudo apt install tree git

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to log on to Ubuntu. If you're running another OS, amend the installation command
accordingly.


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a subdirectory for
each chapter. Get the course repo initially by a `git clone` operation

![Git Clone](img/git-clone.png)

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cxx-course/gitlab
    git pull


# Build Solution/Demo Programs
The solutions and demo programs are all using CMake as the build tool. CMake is a cross-platform generator
tool that can generate makefiles and other build tool files. It is also the project descriptor for JetBrains
CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources.
What you do need to have; are `cmake`, `make` and `gcc/g++` all installed.
When you want to build a solution or demo:

First change into its project directory `cd path/to/some/solutions/dir`, then run the commands below
and the executable will be in the `./bld/` directory.

    cmake -S . -B bld
    cmake --build bld
