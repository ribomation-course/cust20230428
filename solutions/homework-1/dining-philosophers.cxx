#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "chopstick.hxx"
#include "philosopher.hxx"

using std::cout;
using std::osyncstream;
namespace rm = ribomation::concurrent;

int main(int argc, char** argv) {
    auto no_deadlock = (argc > 1 && argv[1] == std::string{"--no-deadlock"});
    auto const P     = (argc > 2 ? std::stoi(argv[2]) : 5U);

    auto chopsticks   = std::vector<rm::Chopstick>(P);
    auto philosophers = std::vector<std::jthread>{};
    philosophers.reserve(P);

    for (auto k = 0U; k < P; ++k) {
        auto rightIdx = k;
        auto leftIdx  = (k + 1) % P;

        //This solution breaks Coffman's condition: Circular wait
        if (no_deadlock && k == 0) std::swap(rightIdx, leftIdx);

        philosophers.emplace_back(rm::Philosopher{k + 1, chopsticks[rightIdx], chopsticks[leftIdx]});
    }
}
