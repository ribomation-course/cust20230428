#pragma once

#include <iostream>
#include <string>
#include <syncstream>
#include "chopstick.hxx"

namespace ribomation::concurrent {
    using std::string;
    using std::cout;
    using std::osyncstream;
    using namespace std::chrono_literals;

    class Philosopher {
        unsigned const id;
        Chopstick& right;
        Chopstick& left;
    public:
        Philosopher(unsigned id_, Chopstick& right_, Chopstick& left_)
                : id{id_}, right{right_}, left{left_} {}

        [[noreturn]] void operator ()() {
            auto const tab = string(15 * (id - 1), ' ');
            do {
                osyncstream{cout} << tab << "[" << id << "] Want RIGHT\n";
                right.acquire();

                osyncstream{cout} << tab << "[" << id << "] Want LEFT\n";
                left.acquire();

                osyncstream{cout} << tab << "[" << id << "] EATING\n";
                std::this_thread::sleep_for(.5s);

                osyncstream{cout} << tab << "[" << id << "] Releasing sticks\n";
                left.release();
                right.release();
            } while (true);
        }
    };

}

