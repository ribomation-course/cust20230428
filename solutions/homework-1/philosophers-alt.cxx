#include <iostream>
#include <syncstream>
#include <vector>
#include <string>
#include <thread>
#include <mutex>

using namespace std::chrono_literals;
using std::cout;
using std::osyncstream;

class Philosopher {
    unsigned const id;
    std::mutex& left;
    std::mutex& right;
public:
    Philosopher(unsigned id_, std::mutex& left_, std::mutex& right_)
    : id{id_}, left{left_}, right{right_} {}

    [[noreturn]] void operator ()() {
        auto const tab = std::string(15 * (id - 1), ' ');
        do {
            osyncstream{cout} << tab << "[" << id << "] hungry\n";
            {
                //This solution breaks Coffman's condition: Hold and wait
                auto g = std::scoped_lock{left, right};

                osyncstream{cout} << tab << "[" << id << "] EATING\n";
                std::this_thread::sleep_for(.5s);
            }
            osyncstream{cout} << tab << "[" << id << "] thinking\n";
            std::this_thread::sleep_for(.5s);
        } while (true);
    }
};

int main() {
    auto const P = 5U;
    auto chopsticks   = std::vector<std::mutex>(P);
    auto philosophers = std::vector<std::jthread>{};
    philosophers.reserve(P);

    for (auto k = 0U; k < P; ++k) {
        auto rightIdx = k;
        auto leftIdx  = (k + 1) % P;

        philosophers.emplace_back(Philosopher{k + 1, chopsticks[rightIdx], chopsticks[leftIdx]});
    }
}

