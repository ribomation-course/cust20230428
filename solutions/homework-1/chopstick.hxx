#pragma once
#include <mutex>
#include <condition_variable>

namespace ribomation::concurrent {

    class Chopstick {
        bool busy = false;
        std::mutex exclusive{};
        std::condition_variable not_busy{};
    public:
        void acquire() {
            auto g = std::unique_lock{exclusive};
            not_busy.wait(g, [this] { return !busy; });
            busy = true;
        }

        void release() {
            {
                auto g = std::lock_guard{exclusive};
                busy = false;
            }
            not_busy.notify_all();
        }
    };

}
