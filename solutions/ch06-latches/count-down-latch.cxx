#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <vector>
#include <thread>
#include <latch>
#include <syncstream>
const auto msg = std::string{"Schools out, it's summer!"};
namespace cr = std::chrono;
using std::cout;

void SLEEP() {
    static auto R = std::random_device{};
    static auto r = std::default_random_engine{R()};
    static auto sleep_time = std::uniform_int_distribution<int>{1000, 4000}(r);
    std::this_thread::sleep_for(cr::milliseconds(sleep_time));
}

int main() {
    auto const W = 40U;
    auto done = std::latch{W};

    auto worker = [&done](unsigned id) {
        auto const tab = std::string(id * 3, ' ');
        std::osyncstream{cout} << tab << "[" << id << "] CREATED\n";
        SLEEP();

        std::osyncstream{cout} << tab << "WAITING\n";
        done.arrive_and_wait();

        std::osyncstream{cout} << tab << "DONE\n";
    };

    std::osyncstream{cout} << "[Main] Creating workers...\n";
    {
        auto workers = std::vector<std::jthread>{};
        workers.reserve(W);
        for (auto id = 1U; id <= W; ++id) workers.emplace_back(worker, id);
        std::osyncstream{cout} << "[Main] waiting...\n";
        done.wait();
    }

    std::osyncstream{cout} << "[Main] " << msg << "\n";
}
