#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <filesystem>
#include <array>
#include <numeric>
#include <sys/wait.h>
#include "shared-memory.hxx"

using namespace std::string_literals;
using namespace std::string_view_literals;
namespace fs = std::filesystem;
namespace rm = ribomation::concurrent;
using std::cout;
using std::string_view;

unsigned count(string_view chunk, string_view phrase) {
    unsigned cnt = 0;
    for (auto it = chunk.find(phrase);
         it != string_view::npos;
         it = chunk.find(phrase, it + phrase.size())) {
        ++cnt;
    }
    return cnt;
}

int main() {
    constexpr auto W = 10U;
    using Counts     = std::array<unsigned, W>;

    auto filename    = "../shakespeare.txt"s;
    auto phrase      = "Hamlet"sv;

    auto fsize   = fs::file_size(filename);
    auto shm     = rm::SharedMemory{fsize + sizeof(Counts)};
    auto counts  = new (shm.allocate<Counts>()) Counts{};
    auto payload = string_view{reinterpret_cast<char*>(shm.allocate(fsize)), fsize};
    auto file    = std::ifstream{filename};
    file.read(const_cast<char*>(payload.data()), static_cast<std::streamsize>(payload.size()));

    auto chunkSize = fsize / W;
    for (auto id = 0U; id < W; ++id) {
        if (fork() == 0) {
            auto chunk = payload.substr(id * chunkSize, chunkSize);
            cout << "worker-" << (id + 1) << " started\n";
            counts->at(id) = count(chunk, phrase);
            cout << "worker-" << (id + 1) << " count=" << counts->at(id) << "\n";
            exit(0);
        }
    }

    for (auto id = 0U; id < W; ++id) {
        ::wait(nullptr);
    }
    
    auto cnt = std::accumulate(counts->begin(), counts->end(), 0U);
    cout << "result: " << phrase << ": " << cnt << "\n";
}
