#include <iostream>
#include <stdexcept>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

class Process {
public:
    Process() = default;
    virtual ~Process() = default;

    void start() {
        auto rc = ::fork();
        if (rc < 0) {
            throw std::runtime_error{"fork() failed"};
        }
        pid = rc;
        if (rc > 0) return;
        run();
        exit(0);
    }
    void join() const {
        auto rc = ::waitpid(pid, nullptr, 0);
        if (rc < 0) {
            throw std::runtime_error{"waitpid() failed"};
        }
    }

protected:
    virtual void run() = 0;

private:
    int pid{};
};


class Kid : public Process {
    unsigned N;
public:
    explicit Kid(unsigned N) : N(N) {
        start();
    }
protected:
    void run() override {
        auto pid = ::getpid();
        for (auto k = 0U; k < N; ++k) {
            std::cout << "Hello from child " << pid << "\n";
            usleep(10 * 1000);
        }
        std::cout << "Child " << pid << " done\n";
    }
};

int main() {
    auto k1 = Kid{10};
    auto k2 = Kid{15};
    auto k3 = Kid{20};
    k3.join();
    k2.join();
    k1.join();
}
