cmake_minimum_required(VERSION 3.22)
project(ch14_processes)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(make-kids make-kids.cxx)
target_compile_options(make-kids PRIVATE ${WARN})


