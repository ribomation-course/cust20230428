#pragma once
#include <array>
#include <mutex>
#include <condition_variable>
#include <iostream>

namespace ribomation::concurrent {

    template<typename MessageType, unsigned CAPACITY = 16U>
    class MessageQueueBounded {
        std::array<MessageType, CAPACITY> inbox{};
        std::mutex exclusive{};
        std::condition_variable not_empty{};
        std::condition_variable not_full{};
        int putIdx = 0;
        int getIdx = 0;
        int size   = 0;
    public:
        MessageQueueBounded() = default;
        MessageQueueBounded(MessageQueueBounded const&) = delete;
        auto operator =(MessageQueueBounded const&) -> MessageQueueBounded& = delete;

        bool empty() const { return size == 0; }
        bool full() const { return static_cast<unsigned long>(size) == inbox.max_size(); }

        void put(MessageType msg) {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_full.wait(guard, [this] { return not full(); });
            inbox[putIdx++] = msg;
            putIdx %= inbox.max_size();
            ++size;
            not_empty.notify_all();
        }

        MessageType get() {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_empty.wait(guard, [this] { return not empty(); });
            auto msg = inbox[getIdx++];
            getIdx %= inbox.max_size();
            --size;
            not_full.notify_all();
            return msg;
        }
    };

}

