#pragma once
#include <functional>
#include <pthread.h>

namespace ribomation::concurrent {

    class Thread {
        pthread_t runner{};
        std::function<void()> body;
        static void* wrapper(Thread* t) {
            t->body();
            return nullptr;
        }

    public:
        explicit Thread(std::function<void()> body_) : body{std::move(body_)} {
            pthread_create(&runner, nullptr, reinterpret_cast<void* (*)(void*)>(wrapper), this);
        }
        ~Thread() { join(); }
        void join() const { pthread_join(runner, nullptr); }
    };

}

