#pragma once

#include <string>
#include <fstream>
#include <stdexcept>

namespace ribomation::io {
    using namespace std::string_literals;
    using std::string;

    class FileSlurper {
        std::ifstream file;
        unsigned const maxLines;

    public:
        explicit FileSlurper(string const& filename, unsigned maxLines_ = 0) : file{filename}, maxLines{maxLines_} {
            if (!file) throw std::invalid_argument("cannot open "s + filename);
        }

        class iterator {
            FileSlurper* slurper;
            string currentLine{};
            unsigned linesRead = 0;

            string readLine() {
                string line;
                getline(slurper->file, line);
                ++linesRead;
                return line;
            }

        public:
            iterator(FileSlurper* slurper_) : slurper{slurper_} {
                if (slurper != nullptr) currentLine = readLine();
            }

            void operator ++() { currentLine = readLine(); }
            string operator *() const { return currentLine; }
            bool operator !=(const iterator&) const {
                if (slurper->maxLines > 0 && linesRead > slurper->maxLines) return false;
                return !slurper->file.eof();
            }
        };

        iterator begin() { return {this}; }
        iterator end() { return {nullptr}; }
    };
}
