#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#include <syncstream>
#include <thread>
#include <utility>
#include "receivable.hxx"
#include "rendezvous.hxx"
#include "file-slurper.hxx"

namespace co = ribomation::concurrent;
namespace io = ribomation::io;
using namespace std::string_literals;
using std::string;
using std::cout;
using std::osyncstream;

using Message = co::RendezvousMessage<string, string>;

auto toUpper(string s) {
    std::transform(s.begin(), s.end(), s.begin(), [](char ch) {
        return ::toupper(ch);
    });
    return s;
}

struct Server : co::Receivable<Message*> {
    bool done = false;
    auto compute(string const& arg) -> string {
        auto msg = Message{arg};
        put(&msg);
        return msg.promise.get_future().get();
    }

    void run() {
        do {
            auto msg = get();
            auto str = msg->payload;
            if (done && str.empty()) {
                msg->promise.set_value(""s);
                break;
            }
            msg->promise.set_value(toUpper(str));
        } while (true);
        cout << "[server] done\n";
    }
};

struct Client {
    unsigned id;
    string const filename;
    unsigned maxLines;
    Server& server;

    Client(unsigned id, string  filename, unsigned maxLines, Server& server)
            : id(id), filename(std::move(filename)), maxLines(maxLines), server(server) {}

    void run() const {
        osyncstream{cout} << "[client-" << id << "] started\n";
        decltype(maxLines) lineno{};
        for (auto line: io::FileSlurper{filename, maxLines}) {
            if (line.empty()) { ++lineno; continue; }
            osyncstream{cout} << "[client-" << id << "] ("<< ++lineno <<") \"" << server.compute(line) << "\"\n";
        }
        osyncstream{cout} << "[client-" << id << "] done\n";
    }
};


int main() {
    auto server = Server{};
    auto serverThr = std::jthread{&Server::run, &server};

    auto const N = 1000U;
    {
        auto client1 = Client{1, "../musketeers.txt"s, N, server};
        auto client1Thr = std::jthread{&Client::run, &client1};

        auto client2 = Client{2, "../shakespeare.txt"s, N, server};
        auto client2Thr = std::jthread{&Client::run, &client2};
    }

    cout << "[main] before done\n";
    server.done = true;
    server.compute(""s);
    cout << "[main] done\n";
}
