#pragma once
#include <future>

namespace ribomation::concurrent {

    template<typename PayloadType, typename ReplyType>
    struct RendezvousMessage {
        PayloadType payload;
        std::promise<ReplyType> promise;

        explicit RendezvousMessage(PayloadType payload) : payload(payload) {}
    };

}
