#include <iostream>
#include <thread>
#include <syncstream>
#include "receivable.hxx"

namespace rm = ribomation::concurrent;
using std::cout;
using std::osyncstream;

auto const TAB = 5U;

struct Consumer : rm::Receivable<long> {
    void run() {
        auto const prefix = std::string(3 * TAB, ' ') + "[cons] ";
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << prefix << msg << "\n";
        }
        osyncstream{cout} << prefix<< "done\n";
    }
};

struct Transformer : rm::Receivable<long> {
    unsigned const id;
    rm::Receivable<long>* next = nullptr;

    explicit Transformer(unsigned id_, rm::Receivable<long>* next) : id{id_}, next{next} {}

    void run() {
        auto const prefix = std::string(id * TAB, ' ') + "[tran-" + std::to_string(id) + "] ";
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << prefix << msg << "\n";
            next->send(2 * msg);
        }
        next->send(0);
        osyncstream{cout} << prefix << "done\n";
    }
};

struct Producer {
    unsigned const N;
    rm::Receivable<long>* next = nullptr;

    Producer(unsigned n, rm::Receivable<long>* next) : N(n), next(next) {}

    void run() const {
        for (auto msg = 1U; msg <= N; ++msg) {
            osyncstream{cout} << "[prod] msg:" << msg << "\n";
            next->send(msg);
        }
        next->send(0);
    }
};

int main() {
    auto const N = 10'000;
    auto cons   = Consumer{};
    auto tran2  = Transformer{2, &cons};
    auto tran1  = Transformer{1, &tran2};
    auto prod   = Producer{N, &tran1};
    {
        auto consThr  = std::jthread{&Consumer::run, &cons};
        auto tran2Thr = std::jthread{&Transformer::run, &tran2};
        auto tran1Thr = std::jthread{&Transformer::run, &tran1};
        auto prodThr  = std::jthread{&Producer::run, &prod};
    }
    cout << "[main] done\n";
}
