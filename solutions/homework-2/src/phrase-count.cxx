#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <string_view>
#include <stdexcept>
#include <chrono>
#include <memory>
#include <tuple>
#include <vector>
#include <numeric>
#include "thread-pool.hxx"

namespace fs = std::filesystem;
namespace cr = std::chrono;
namespace co = ribomation::concurrent;
using namespace std::string_literals;
using namespace std::string_view_literals;
using std::cout;

void parseArgs(int argc, char** argv, fs::path& filename, std::string_view& phrase, unsigned& tasks);
auto load(fs::path const& filename) -> std::tuple<std::string_view, std::unique_ptr<char>>;
auto count(std::string_view payload, std::string_view phrase, unsigned numTasks) -> unsigned;

int main(int argc, char** argv) {
    auto filename = fs::path{"../musketeers.txt"};
    auto phrase = "Aramis"sv;
    auto numTasks = 25U;
    parseArgs(argc, argv, filename, phrase, numTasks);

    auto [payload, mem] = load(filename);
    cout << "loaded : " << payload.size() << " bytes\n";

    auto startTime = cr::high_resolution_clock::now();

    auto phraseCount = count(payload, phrase, numTasks);
    cout << "phrase '" << phrase << "' occurs " << phraseCount << " times\n";

    auto endTime = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "elapsed " << elapsed.count() << " ms\n";
}

void parseArgs(int argc, char** argv, fs::path& filename, std::string_view& phrase, unsigned& tasks) {
    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string_view{argv[k]};
        if (arg == "--file"sv) filename = argv[++k];
        else if (arg == "--phrase"sv) phrase = argv[++k];
        else if (arg == "--tasks"sv) tasks = std::stoi(argv[++k]);
        else {
            std::cerr << "usage: " << argv[0] << " [--file <filename>] [--phrase <string>] [--tasks <int>]\n";
            throw std::invalid_argument{"invalid argument: "s + argv[k]};
        }
    }

    cout << "file   : " << filename << "\n";
    cout << "phrase : " << phrase << "\n";
    cout << "# tasks: " << tasks << "\n";
}

auto load(fs::path const& filename) -> std::tuple<std::string_view, std::unique_ptr<char>> {
    auto size = fs::file_size(filename);
    auto buf = new char[size];
    auto file = std::ifstream{filename};
    if (!file) throw std::invalid_argument{"cannot open "s + filename.string()};
    file.read(buf, static_cast<std::streamsize>(size));
    return std::make_tuple(std::string_view{buf, size}, std::unique_ptr<char>{buf});
}

auto count(std::string_view payload, std::string_view phrase, unsigned numTasks) -> unsigned {
    auto chunkSize = payload.size() / numTasks;
    cout << "chunk  : " << chunkSize << " bytes\n";

    auto pool = co::ThreadPool<unsigned>{};
    auto results = std::vector<std::shared_future<unsigned>>{};
    results.reserve(numTasks);
    for (auto k = 0U; k < numTasks; ++k) {
        auto chunk = payload.substr(k * chunkSize, chunkSize);
        auto fut = pool.submit([chunk, phrase] {
            auto count = 0U;
            for (auto idx = chunk.find(phrase);
                 idx != std::string_view::npos;
                 idx = chunk.find(phrase, idx + phrase.size())) {
                ++count;
            }
            return count;
        });
        results.push_back(fut);
    }

    return std::accumulate(results.begin(), results.end(), 0U, [](auto sum, auto&& r) {
        return sum + r.get();
    });
}

