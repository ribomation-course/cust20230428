#include <iostream>
#include <iomanip>
#include <fstream>
#include <filesystem>
#include <string>
#include <string_view>
#include <chrono>
#include <memory>
#include <tuple>
#include <vector>
#include <numeric>
#include "pstream.h"
#include "decompressor.h"
#include "thread-pool.hxx"
#include "mmap/memory-mapped-file.hxx"
#include "mmap/memory-block.hxx"


namespace fs = std::filesystem;
namespace cr = std::chrono;
namespace co = ribomation::concurrent;
namespace io = ribomation::io;
using namespace std::string_literals;
using namespace std::string_view_literals;
using std::cout;

void parseArgs(int argc, char** argv, fs::path& filename, std::string_view& phrase, unsigned& tasks, bool& uncompress);
auto getUncompressedSize(fs::path const& filename) -> unsigned long;
auto count(std::string_view payload, std::string_view phrase, unsigned numTasks) -> unsigned;


int main(int argc, char** argv) {
    auto filename = fs::path{"../../../files/large-text-98mb.txt.gz"};
    auto phrase = "Aramis"sv;
    auto numTasks = 25U;
    auto uncompress = false;
    parseArgs(argc, argv, filename, phrase, numTasks, uncompress);

    auto file = io::MemoryMappedFile{filename};
    if (uncompress) {
        auto uncompressedSize = getUncompressedSize(filename);
        auto blk = io::MemoryBlock{uncompressedSize};
        auto decompressor = Decompressor{};
        decompressor.Feed(file.data().data(), file.data().size(), (unsigned char*) blk.data().data(), blk.data().size(), false);
        file = std::move(blk);
    }

    auto startTime = cr::high_resolution_clock::now();

    auto phraseCount = count(file.data(), phrase, numTasks);
    cout << "phrase '" << phrase << "' occurs " << phraseCount << " times\n";

    auto endTime = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "elapsed " << elapsed.count() << " ms\n";
}

void parseArgs(int argc, char** argv, fs::path& filename, std::string_view& phrase, unsigned& tasks, bool& uncompress) {
    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string_view{argv[k]};
        if (arg == "--file"sv) filename = argv[++k];
        else if (arg == "--phrase"sv) phrase = argv[++k];
        else if (arg == "--tasks"sv) tasks = std::stoi(argv[++k]);
        else {
            std::cerr << "usage: " << argv[0] << " [--file <filename>] [--phrase <string>] [--tasks <int>]\n";
            throw std::invalid_argument{"invalid argument: "s + argv[k]};
        }
    }

    if (filename.extension() == ".gz") {
        uncompress = true;
    }

    cout << "file   : " << filename << "\n";
    cout << "phrase : " << phrase << "\n";
    cout << "# tasks: " << tasks << "\n";
}

auto getUncompressedSize(fs::path const& filename) -> unsigned long {
    //$ gzip --list large-text-98mb.txt.gz
    //        compressed        uncompressed  ratio uncompressed_name
    //        37418380           102294495  63.4% large-text-98mb.txt
    auto pipe = redi::ipstream("gzip --list "s + filename.string());
    auto line = std::string{};
    std::getline(pipe, line);
    auto compressedSize = 0UL;
    auto uncompressedSize = 0UL;
    pipe >> compressedSize >> uncompressedSize;
    cout << "compressed  : " << std::setw(10) << std::right << compressedSize << " bytes\n";
    cout << "uncompressed: " << std::setw(10) << std::right << uncompressedSize << " bytes\n";
    return uncompressedSize;
}

auto count(std::string_view payload, std::string_view phrase, unsigned numTasks) -> unsigned {
    auto chunkSize = payload.size() / numTasks;
    cout << "chunk  : " << chunkSize << " bytes\n";

    auto pool = co::ThreadPool<unsigned>{};
    auto results = std::vector<std::shared_future<unsigned>>{};
    results.reserve(numTasks);
    for (auto k = 0U; k < numTasks; ++k) {
        auto chunk = payload.substr(k * chunkSize, chunkSize);
        auto fut = pool.submit([chunk, phrase] {
            auto count = 0U;
            for (auto idx = chunk.find(phrase);
                 idx != std::string_view::npos;
                 idx = chunk.find(phrase, idx + phrase.size())) {
                ++count;
            }
            return count;
        });
        results.push_back(fut);
    }

    return std::accumulate(results.begin(), results.end(), 0U, [](auto sum, auto&& r) {
        return sum + r.get();
    });
}



