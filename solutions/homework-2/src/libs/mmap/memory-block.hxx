#pragma once

#include <string_view>
#include <stdexcept>
#include <cstring>
#include <sys/types.h>
#include <sys/mman.h>


namespace ribomation::io {
    using namespace std::string_literals;

    class MemoryBlock {
        char* payload = nullptr;
        size_t payloadSize = 0;

        friend class MemoryMappedFile;

    public:
        explicit MemoryBlock(unsigned long size) {
            payloadSize = size;
            payload = (char*) ::mmap(nullptr, payloadSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
            if (payload == MAP_FAILED) throw std::runtime_error("failed to mmap(): "s + strerror(errno));
        }

        ~MemoryBlock() {
            if (payloadSize > 0) {
                munmap(reinterpret_cast<void*>(payload), payloadSize);
            }
        }

        [[nodiscard]] auto size() const {
            return payloadSize;
        }

        auto data() {
            return std::string_view{payload, payloadSize};
        }
    };

}