#pragma once

#include <iostream>
#include <filesystem>
#include <string>
#include <string_view>
#include <stdexcept>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "memory-block.hxx"


namespace ribomation::io {
    namespace fs = std::filesystem;
    using namespace std::string_literals;
    using std::string;

    class MemoryMappedFile {
        char* payload;
        size_t payloadSize;

    public:
        explicit MemoryMappedFile(string const& filename) {
            if (!fs::exists(filename)) throw std::invalid_argument(filename + " not found"s);
            int fd = ::open(filename.c_str(), O_RDWR);
            if (fd < 0) throw std::invalid_argument{"cannot open '"s + filename + "': "s + strerror(errno)};

            payloadSize = fs::file_size(filename);
            payload = (char*) ::mmap(nullptr, payloadSize, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
            if (payload == MAP_FAILED) throw std::runtime_error("failed to mmap(): "s + strerror(errno));
            ::close(fd);
        }

        MemoryMappedFile(MemoryMappedFile const&) = delete;
        MemoryMappedFile& operator=(MemoryMappedFile const&) = delete;
        MemoryMappedFile(MemoryMappedFile&& rhs) noexcept = delete;
        MemoryMappedFile& operator=(MemoryMappedFile&& rhs) noexcept = delete;

        MemoryMappedFile& operator=(MemoryBlock&& rhs) noexcept {
            munmap(reinterpret_cast<void*>(payload), payloadSize);
            payload = rhs.payload;
            rhs.payload = nullptr;
            payloadSize = rhs.payloadSize;
            rhs.payloadSize = 0;
            return *this;
        }

        ~MemoryMappedFile() {
            munmap(reinterpret_cast<void*>(payload), payloadSize);
        }

        [[nodiscard]] auto size() const {
            return payloadSize;
        }

        auto data() {
            return std::string_view{payload, payloadSize};
        }
    };

}

