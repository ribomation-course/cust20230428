#pragma once

#include <deque>
#include <mutex>
#include <condition_variable>

namespace ribomation::concurrent {

    template<typename MessageType>
    class MessageQueue {
        std::deque<MessageType> inbox{};
        std::mutex exclusive{};
        std::condition_variable not_empty{};
        bool is_cleared = false;

    public:
        MessageQueue() = default;
        MessageQueue(MessageQueue const&) = delete;
        MessageQueue(MessageQueue&&) = delete;
        auto operator=(MessageQueue const&) -> MessageQueue& = delete;
        auto operator=(MessageQueue&&) -> MessageQueue& = delete;

        void put(MessageType msg) {
            {
                auto guard = std::lock_guard<std::mutex>{exclusive};
                inbox.push_back(std::move(msg));
            }
            not_empty.notify_all();
        }

        MessageType get() {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_empty.wait(guard, [this] { return not inbox.empty(); });
            if (is_cleared) {
                return MessageType{};
            }
            auto msg = std::move(inbox.front());
            inbox.pop_front();
            return msg;
        }
    };

}


