#include <stdio.h>
#include <pthread.h>

typedef struct {
    pthread_mutex_t exclusive;
    pthread_cond_t not_empty;
    pthread_cond_t not_full;
    int full;
    int payload;
} Mailbox;

void mbox_construct(Mailbox* this) {
    pthread_mutex_init(&(this->exclusive), NULL);
    pthread_cond_init(&(this->not_empty), NULL);
    pthread_cond_init(&(this->not_full), NULL);
    this->full = 0;
    this->payload = 0;
}

void mbox_destroy(Mailbox* this) {
    pthread_mutex_destroy(&(this->exclusive));
    pthread_cond_destroy(&(this->not_empty));
    pthread_cond_destroy(&(this->not_full));
}

void mbox_put(Mailbox* this, int x) {
    pthread_mutex_lock(&(this->exclusive));
    while (this->full) {
        pthread_cond_wait(&(this->not_full), &(this->exclusive));
    }
    this->payload = x;
    this->full = 1;
    pthread_cond_broadcast(&(this->not_empty));
    pthread_mutex_unlock(&(this->exclusive));
}

int mbox_get(Mailbox* this) {
    pthread_mutex_lock(&(this->exclusive));
    while (!(this->full)) {
        pthread_cond_wait(&(this->not_empty), &(this->exclusive));
    }
    int x = this->payload;
    this->full = 0;
    pthread_cond_broadcast(&(this->not_full));
    pthread_mutex_unlock(&(this->exclusive));
    return x;
}

void* consumer_body(void* arg) {
    Mailbox* mb = (Mailbox*) arg;
    for (int msg = mbox_get(mb);
         msg != 0;
         msg = mbox_get(mb)) {
        printf("[cons] %d\n", msg);
    }
    return NULL;
}

void* producer_body(void* arg) {
    Mailbox* mb = (Mailbox*) arg;
    for (int msg = 1; msg <= 1000; ++msg) {
        mbox_put(mb, msg);
    }
    mbox_put(mb, 0);
    return NULL;
}

int main() {
    Mailbox mbox;
    mbox_construct(&mbox);

    pthread_t consumerId;
    pthread_create(&consumerId, NULL, &consumer_body, &mbox);

    pthread_t producerId;
    pthread_create(&producerId, NULL, &producer_body, &mbox);

    pthread_join(producerId, NULL);
    pthread_join(consumerId, NULL);
    mbox_destroy(&mbox);
    printf("[main] exit\n");
}
