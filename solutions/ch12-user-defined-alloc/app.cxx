#include <iostream>
#include <numbers>
#include <string>
using std::cout;

template<unsigned CAPACITY>
class Memory {
    using byte = unsigned char;
    byte storage[CAPACITY]{};
    byte* nextAddr = storage;
public:
    bool full() const {
        return nextAddr >= storage + CAPACITY;
    }
    void* alloc(unsigned numBytes) {
        auto addr = nextAddr;
        nextAddr += numBytes;
        return addr;
    }
};

struct Person {
    std::string name{};
    unsigned age{};
    Person(std::string name_, unsigned age_) : name{std::move(name_)}, age{age_} {}
};
auto operator <<(std::ostream& os, Person const& p) -> std::ostream& {
    return os << "Person{" << p.name << ", " << p.age << "}";
}

int main() {
    auto m = Memory<1000>{};

    auto intPtr = new (m.alloc(sizeof(int))) int{42};
    cout << "int: " << *intPtr << " @ " << intPtr << "\n";

    auto fltPtr = new (m.alloc(sizeof(long double))) long double{std::numbers::pi};
    cout << "flt: " << *fltPtr << " @ " << fltPtr << "\n";

    auto prsPtr = new (m.alloc(sizeof(Person))) Person{"Nisse", 37};
    cout << "prs: " << *prsPtr << " @ " << prsPtr << "\n";
}
