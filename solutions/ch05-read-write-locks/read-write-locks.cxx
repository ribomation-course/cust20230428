#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <syncstream>

using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;
using std::osyncstream;
using std::string;

class Ticker {
    long value{};
    mutable std::shared_mutex rwl{};
public:
    auto read() const {
        auto g = std::shared_lock<std::shared_mutex>{rwl};
        return value;
    }

    auto increment() {
        auto g = std::unique_lock<std::shared_mutex>{rwl};
        return ++value;
    }

    auto decrement() {
        auto g = std::unique_lock<std::shared_mutex>{rwl};
        return --value;
    }
};

class Reader {
    const unsigned id;
    Ticker& ticker;
    unsigned long seed;
public:
    Reader(const unsigned int id, Ticker& ticker, unsigned long seed) : id(id), ticker(ticker), seed(seed) {}

    [[noreturn]] void operator ()() {
        auto r = std::default_random_engine{seed};
        auto ms = std::uniform_int_distribution<long>{400, 1000};
        auto const tab = string(id * 4, ' ');
        do {
            osyncstream{cout} << tab << "[" << id << "] " << ticker.read() << "\n";
            std::this_thread::sleep_for(cr::milliseconds(ms(r)));
        } while (true);
    }
};

class Incrementer {
    Ticker& ticker;
public:
    explicit Incrementer(Ticker& ticker) : ticker(ticker) {}

    [[noreturn]] void operator ()() {
        do {
            std::this_thread::sleep_for(2s);
            osyncstream{cout} << "[INC] " << ticker.increment() << " ------------\n";
        } while (true);
    }
};

class Decrementer {
    Ticker& ticker;
public:
    explicit Decrementer(Ticker& ticker) : ticker(ticker) {}

    [[noreturn]] void operator ()() {
        do {
            std::this_thread::sleep_for(3s);
            osyncstream{cout} << "[DEC] " << ticker.decrement() << " ------------\n";
        } while (true);
    }
};

int main() {
    auto R = std::random_device{};
    auto ticker = Ticker{};
    auto incThr = std::jthread{Incrementer{ticker}};
    auto decThr = std::jthread{Decrementer{ticker}};
    auto reader1 = std::jthread{Reader{1, ticker, R()}};
    auto reader2 = std::jthread{Reader{2, ticker, R()}};
    auto reader3 = std::jthread{Reader{3, ticker, R()}};
    auto reader4 = std::jthread{Reader{4, ticker, R()}};
}

