#include <iostream>
#include <vector>
#include <syncstream>
#include <thread>
#include <mutex>

using namespace std::chrono_literals;
using std::cout;
using std::osyncstream;

class Data {
    long shared_value = 0;
    mutable std::mutex exclusive{};
public:
    void increment() {
        auto g = std::lock_guard<std::mutex>{exclusive};
        ++shared_value;
    }

    void decrement() {
        auto g = std::lock_guard<std::mutex>{exclusive};
        --shared_value;
    }

    auto value() const {
        auto g = std::lock_guard<std::mutex>{exclusive};
        return shared_value;
    }
};

int main() {
    auto const N = 100'000U;
    auto the_data = Data{};

    auto body = [&the_data, n = N](unsigned id) mutable {
        osyncstream{cout} << "    [" << id << "] enter\n";
        do {
            the_data.increment();
            the_data.decrement();
            //std::this_thread::sleep_for(.1s);
        } while (--n > 0);
        osyncstream{cout} << "    [" << id << "] done\n";
    };

    cout << "[main] (a) value = " << the_data.value() << "\n";
    {
        auto const T = 100U;
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) threads.emplace_back(body, id);
        cout << "[main] (b) value = " << the_data.value() << "\n";
    }
    cout << "[main] (c) value = " << the_data.value() << "\n";
}
