#pragma once
#include <mutex>
#include <condition_variable>

namespace ribomation::concurrent {

    template<typename PayloadType>
    class Mailbox {
        std::mutex exclusive{};
        std::condition_variable not_empty{};
        std::condition_variable not_full{};
        PayloadType payload{};
        bool full = false;
    public:
        void send(PayloadType x) {
            {
                auto guard = std::unique_lock<std::mutex>{exclusive};
                not_full.wait(guard, [this] { return !full; });
                payload = x;
                full = true;
            }
            not_empty.notify_one();
        }

        PayloadType recv() {
            PayloadType x{};
            {
                auto guard = std::unique_lock<std::mutex>{exclusive};
                not_empty.wait(guard, [this] { return full; });
                x = payload;
                payload = {};
                full = false;
            }
            not_full.notify_one();
            return x;
        }
    };

}

