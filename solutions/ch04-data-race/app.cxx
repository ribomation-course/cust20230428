#include <iostream>
#include <thread>
#include <syncstream>
#include "mailbox.hxx"

using std::cout;
using std::osyncstream;
namespace rm = ribomation::concurrent;

int main() {
    cout << "[main] enter\n";
    {
        auto const N = 10'000U;
        auto box = rm::Mailbox<long>{};

        auto consumer = std::jthread{[&box] {
            for (auto msg = box.recv(); msg != -1; msg = box.recv()) {
                osyncstream{cout} << "[consumer] " << msg << "\n";
            }
            osyncstream{cout} << "[consumer] done\n";
        }};

        auto producer = std::jthread{[&box] {
            for (auto k = 1U; k <= N; ++k) box.send(k);
            box.send(-1);
            osyncstream{cout} << "[producer] done\n";
        }};
    }
    cout << "[main] exit\n";
}
