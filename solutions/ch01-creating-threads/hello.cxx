#include <iostream>
#include <string>
#include <thread>
#include <syncstream>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;
using namespace std::string_literals;
namespace cr = std::chrono;
using std::string;
using std::cout;
using std::osyncstream;

auto operator*(string const& s, auto n) -> string {
    string r{};
    while (n-- > 0) r += s;
    return r;
}

void bodyFn(unsigned id, unsigned M) {
    auto const tab = "     "s * (id - 1);
    for (auto k = 1U; k <= M; ++k) {
        osyncstream{cout} << tab << "[func-" << id << "] " << k << "\n";
        std::this_thread::sleep_for(.25s);
    }
}

int main() {
    auto const M = 10U;
    auto const T = std::thread::hardware_concurrency();

    auto bodyLb = [](unsigned id, unsigned M) {
        auto const tab = "     "s * (id - 1);
        for (auto k = 1U; k <= M; ++k) {
            osyncstream{cout} << tab << "[lambda-" << id << "] " << k << "\n";
            std::this_thread::sleep_for(.5s);
        }
    };

    cout << "[main] enter\n";
    {
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) {
            if (id % 2 == 0) {
                threads.emplace_back(bodyLb, id, M);
            } else {
                threads.emplace_back(bodyFn, id, M);
            }
        }
    }
    cout << "[main] exit\n";
}
