#pragma once

#include <mutex>
#include <condition_variable>
#include <array>

namespace ribomation::concurrent {

    template<typename PayloadType, unsigned CAPACITY>
    struct MessageQueue {
        std::array<PayloadType, CAPACITY> queue{};
        unsigned size = 0, putIx = 0, getIx = 0;
        std::mutex exclusive{};
        std::condition_variable not_empty{};
        std::condition_variable not_full{};

        bool empty() const { return size == 0; }

        bool full() const { return size == CAPACITY; }

        void put(PayloadType x) {
            auto g = std::unique_lock{exclusive};
            not_full.wait(g, [this] { return not full(); });
            queue[putIx] = x;
            putIx = (putIx + 1) % CAPACITY;
            ++size;
            not_empty.notify_all();
        }

        auto get() -> PayloadType {
            auto g = std::unique_lock{exclusive};
            not_empty.wait(g, [this] { return not empty(); });
            PayloadType x = queue[getIx];
            getIx = (getIx + 1) % CAPACITY;
            --size;
            not_full.notify_all();
            return x;
        }
    };

}
