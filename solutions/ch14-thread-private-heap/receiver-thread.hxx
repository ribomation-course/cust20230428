#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>
#include <latch>
#include <memory_resource>
#include "message-queue.hxx"


namespace ribomation::concurrent {
    using byte = unsigned char;
    namespace m = std::pmr;

    template<typename MessageType, unsigned THREAD_CAPACITY = 10'000>
    class ReceiverThread {
        std::jthread runner;
        std::latch ready{1};
        m::synchronized_pool_resource* msgHeap = nullptr;
        MessageQueue<MessageType*, 32> inbox{};

        void thread_body() {
            byte storage[THREAD_CAPACITY]{};
            auto buff = m::monotonic_buffer_resource{std::data(storage), std::size(storage), m::null_memory_resource()};
            auto pool = m::synchronized_pool_resource{&buff};
            this->msgHeap = &pool;
            ready.count_down(1);
            application();
        }

    protected:
        virtual void application() = 0;

        auto recv() -> MessageType* { return inbox.get(); }

        void dispose(MessageType* msg) {
            std::destroy_at(msg);
            msgHeap->deallocate(msg, sizeof(MessageType));
        }

    public:
        virtual ~ReceiverThread() = default;
        void join() {
            if (runner.joinable()) {
                runner.join();
            }
        }

        void start() {
            auto kicker = std::jthread{&ReceiverThread::thread_body, this};
            std::swap(kicker, runner);
            ready.wait();
        }

        void send(MessageType msg) {
            auto mem = msgHeap->allocate(sizeof(MessageType));
            auto obj = new(mem) MessageType{msg};
            inbox.put(obj);
        }

        template<typename ... Args>
        void send(Args ... args) {
            auto mem = msgHeap->allocate(sizeof(MessageType));
            auto obj = new(mem) MessageType{args...};
            inbox.put(obj);
        }
    };

}
