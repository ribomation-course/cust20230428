#include <iostream>
#include <syncstream>
#include <thread>
#include <string>
#include <random>
#include <vector>
#include "receiver-thread.hxx"

using std::cout;
using std::string;
using std::osyncstream;
using namespace std::string_literals;
namespace m = std::pmr;
namespace cr = std::chrono;
namespace co = ribomation::concurrent;


struct Account {
    string const accno{};
    long double balance{};

    Account(const string& accno, long double balance = 100)
            : accno(accno), balance(balance) {}
};

auto operator<<(std::ostream& os, Account const& a) -> std::ostream& {
    return os << "Account{" << a.accno << ", " << a.balance << " kr}";
}


struct AccountReceiver : co::ReceiverThread<Account> {
    unsigned const id;

    AccountReceiver(unsigned id_) : id{id_} {
        start();
    }

protected:
    void application() override {
        osyncstream{cout} << "recv-" << id << " enter\n";

        for (auto msg = recv(); true; msg = recv()) {
            auto m = *msg;
            dispose(msg);

            osyncstream{cout} << "recv-" << id << ": " << m << "\n";
            if (m.accno == "last"s) break;
            std::this_thread::sleep_for(cr::milliseconds(10));
        }

        osyncstream{cout} << "recv-" << id << " exit\n";
    }
};


int main() {
    auto const N = 100;

    auto R = std::random_device{};
    auto r = std::default_random_engine{R()};
    auto DIGIT = std::uniform_int_distribution<char>{'0', '9'};
    auto ACCNO = [&r, &DIGIT] {
        return "HB"s + DIGIT(r) + DIGIT(r) + "-"s + DIGIT(r) + DIGIT(r) + DIGIT(r);
    };
    auto AMOUNT = std::normal_distribution<long double>{1000, 250};
    auto BALANCE = [&r, &AMOUNT] { return AMOUNT(r); };

    {
        auto ar1 = AccountReceiver{1};
        auto ar2 = AccountReceiver{1};

        for (auto k = 1; k <= N; ++k) {
            if (k % 2 == 1)
                ar1.send(ACCNO(), BALANCE());
            else
                ar2.send(ACCNO(), BALANCE());
        }
        ar1.send("last"s);
        ar2.send("last"s);
    }
    cout << "[main] exit\n";
}

