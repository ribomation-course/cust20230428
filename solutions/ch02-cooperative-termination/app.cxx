#include <iostream>
#include <string>
#include <thread>
#include <syncstream>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;
using namespace std::string_literals;
namespace cr = std::chrono;
using std::string;
using std::cout;
using std::osyncstream;

auto operator*(string const& s, auto n) -> string {
    string r{};
    while (n-- > 0) r += s;
    return r;
}

int main() {
    auto T = std::thread::hardware_concurrency();

    auto body = [](const std::stop_token& st, unsigned id) {
        auto tab = "     "s * id;
        auto cleaner = std::stop_callback{st, [tab, id](){
            osyncstream{cout} << tab << "[thr-" << id << "] stop_callback\n";
        }};

        unsigned cnt = 0;
        do {
            osyncstream{cout} << tab << "[thr-" << id << "] msg: " << ++cnt << "\n";
            std::this_thread::sleep_for(.5s);
            if (st.stop_requested()) {
                osyncstream{cout} << "[thr-" << id << "] done\n";
                return;
            }
        } while (true);
    };

    cout << "[main] enter\n";
    {
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) {
            threads.emplace_back(body, id);
        }
        cout << "[main] --- before sleep ---------------------\n";
        std::this_thread::sleep_for(5s);
        cout << "[main] --- before stop ----------------------\n";
        for (auto&& t: threads) t.request_stop();
        cout << "[main] --- before join ----------------------\n";
        for (auto&& t: threads) t.join();
    }
    cout << "[main] --- exit ---------------------\n";
}
